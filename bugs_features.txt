TODO:
1. Shader definitions
2. Test 0		-> done
        1		-> done
        2		-> done
        3		-> done
        4		-> done
        5		-> done
        rtsl1	-> done
        rtsl2	-> done
        rtsl3	-> done
        6		-> done
        7		-> done
        8		-> done

3. Compound statements -> done
4. function definitions -> done
5. Operator precedence  -. NA
6. int x, y, z; -> done
7. from PDF
8. Automated for extracting output -> Nikita popov -> done


Bugs:
1. f = f* j; -> fixed
2. Missing Statement -> Sphere.rtsl
3. Strange fucking error -> done

Questions:
1. test 1, 3 - Why does an extra statement get printed after every IF - ELSE? -> solved. 
