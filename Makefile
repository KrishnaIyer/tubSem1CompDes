LLVM_VERSION ?= 3.5
CXXFLAGS = -rdynamic $(shell llvm-config-$(LLVM_VERSION) --cxxflags) -g -O0 -std=c++0x

.PHONY: all clean

all: Easwaran_Teruel_Fernandez_4.so

%.so: %.o
	$(CXX) $(CXXFLAGS) -dylib -shared -fPIC  $^ -o $@

clean:
	rm -f *.o *.so *.bc *.ll *.out *.diff

clang:
	clang-3.5 test_4.c -o test_4.bc -c -emit-llvm




